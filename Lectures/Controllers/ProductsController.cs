using Lectures.Data;
using Lectures.Models;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Lectures.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ProductsRepository _productsRepository;
        
        public ProductsController(ProductsRepository productsRepository)
        {
            this._productsRepository = productsRepository;
        }
        
        public IActionResult Index()
        {
            var products = this._productsRepository.GetProducts();
            return View(products);
        }
        
        [HttpPost]
        public IActionResult Remove(string id)
        {
            this._productsRepository.RemoveProduct(id);
            
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View(new ProductViewModel());
        }

        [HttpPost]
        public IActionResult Create(ProductViewModel category)
        {
            this._productsRepository.CreateProduct(category);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(string id)
        {
            var product = this._productsRepository.GetProduct(id);

            if (product == null)
            {
                return RedirectToAction("Index");
            }

            return View(product);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel product)
        {
            this._productsRepository.UpdateProduct(product);
            return RedirectToAction("Index");
        }
    }
}