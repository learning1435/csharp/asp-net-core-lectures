﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Lectures.Data;
using Lectures.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace Lectures.Controllers
{
    public class FilesController : Controller
    {
        private readonly FilesRepository _filesRepository;
        
        public FilesController(FilesRepository filesRepository)
        {
            this._filesRepository = filesRepository;
        }
        
        public IActionResult Index()
        {
            var items = this._filesRepository.GetBuckets();
            return View(items.ToList());
        }

        [HttpPost]
        public async Task<IActionResult> Remove(string name)
        {
            await this._filesRepository.RemoveBucket(name);
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View(new CatalogViewModel
            {
                Name = string.Empty
            });
        }

        [HttpPost]
        public async Task<IActionResult> Create(CatalogViewModel catalog)
        {
            try
            {
                await this._filesRepository.CreateBucket(catalog.Name);
            }
            catch (Exception)
            {
                return View(new CatalogViewModel
                {
                    Name = catalog.Name
                });
            }
            
            return RedirectToAction("Index");
        }

        public IActionResult Files(string name)
        {
            var files = this._filesRepository.GetObjects(name);
            
            return View(files.ToList());
        }

        public IActionResult Download(string name)
        {
            return View("Files");
        }
    }
}