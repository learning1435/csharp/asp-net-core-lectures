﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lectures.Data;
using Lectures.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lectures.Controllers
{
    public class UsersController : Controller
    {
        private readonly UsersRepository _usersRepository;
        private readonly CategoriesRepository _categoriesRepository;

        public UsersController(UsersRepository usersRepository, CategoriesRepository categoriesRepository)
        {
            this._usersRepository = usersRepository;
            this._categoriesRepository = categoriesRepository;
        }

        public IActionResult Index()
        {
            var users = this._usersRepository.GetUsers();

            return View(users);
        }

        public IActionResult Details(int id)
        {
            var user = this._usersRepository.GetUser(id);

            if (user == null)
                return RedirectToAction("Index");

            return View(user);
        }
        
        [HttpPost]
        public IActionResult Remove(int id)
        {
            this._usersRepository.RemoveUser(id);

            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View(new User());
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }

            this._usersRepository.CreateUser(user);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var user = this._usersRepository.GetUser(id);

            if (user == null)
            {
                return RedirectToAction("Index");
            }

            var editUserViewModel = new EditUserViewModel()
            {
                Id = user.Id,
                Login = user.Login,
                CategoryId = user.CategoryID,
                Name = user.Name,
                Categories = this._categoriesRepository.GetCategories()
            };

            return View(editUserViewModel);
        }

        [HttpPost]
        public IActionResult Edit(EditUserViewModel user)
        {
            if (!ModelState.IsValid)
            {
                user.Categories = this._categoriesRepository.GetCategories();
                return View(user);
            }

            this._usersRepository.UpdateUser(user);
            return RedirectToAction("Index");
        }
    }
}