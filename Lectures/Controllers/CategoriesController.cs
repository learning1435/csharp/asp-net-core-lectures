﻿using System.Diagnostics;
using System.Linq;
using Lectures.Data;
using Lectures.Models;
using Lectures.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Neo4j.Driver.V1;
using Neo4jClient;

namespace Lectures.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly CategoriesRepository _categoriesRepository;

        private readonly BoltGraphClient _driver;
        private readonly Database _dbConfig;

        public CategoriesController(CategoriesRepository categoriesRepository, IOptions<Database> databaseConfig)
        {
            this._dbConfig = databaseConfig.Value;
            
            var uri = this._dbConfig. ConnectionString;
            var user = this._dbConfig.User;
            var password = this._dbConfig.Password;
            
            this._categoriesRepository = categoriesRepository;
            this._driver = new BoltGraphClient(uri, user, password);
            this._driver.Connect();
        }

        public IActionResult Index()
        {
            var users = this._categoriesRepository.GetCategories();

            var results = this._driver.Cypher.Match("(x:Person)")
                .Return(x => x)
                .Results;
            
            Debug.WriteLine(results.Count());
            
            return View(users);
        }
        
        [HttpPost]
        public IActionResult Remove(int id)
        {
            this._categoriesRepository.RemoveCategory(id);
            
            return RedirectToAction("Index");
        }

        public IActionResult Create()
        {
            return View(new Category());
        }

        [HttpPost]
        public IActionResult Create(Category category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }

            this._categoriesRepository.CreateCategory(category);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var category = this._categoriesRepository.GetCategory(id);

            if (category == null)
            {
                return RedirectToAction("Index");
            }

            return View(category);
        }

        [HttpPost]
        public IActionResult Edit(Category category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }

            this._categoriesRepository.UpdateCategory(category);
            return RedirectToAction("Index");
        }
    }
}