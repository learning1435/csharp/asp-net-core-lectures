using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Lectures.Models;
using Minio;
using Minio.DataModel;

namespace Lectures.Data
{
    public class FilesRepository
    {
        private static readonly MinioClient MinClient = new MinioClient("127.0.0.1:9000",
            "AKIAIOSFODNN7EXAMPLE",
            "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
        );

        public IEnumerable<CatalogViewModel> GetBuckets()
        {
            var getListBucketsTask = MinClient.ListBucketsAsync();
            var result = new List<CatalogViewModel>();
            
            // Iterate over the list of buckets.
            foreach (Bucket bucket in getListBucketsTask.Result.Buckets)
            {
                result.Add(new CatalogViewModel
                {
                    Name =  bucket.Name,
                    CreatedAt = bucket.CreationDate
                });
            }

            return result;
        }
        
        public async Task CreateBucket(string bucketName)
        {
            try
            {
                await MinClient.MakeBucketAsync(bucketName);
            } 
            catch (Exception e)
            {
                Console.WriteLine("[Bucket]  Exception: {0}", e);
            }
        }
        
        public async Task RemoveBucket(string bucketName)
        {
            try
            {
                await MinClient.RemoveBucketAsync(bucketName);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Bucket]  Exception: {0}", e);
            }
        }
        
        public List<FileViewModel> GetObjects(string bucketName, string prefix = null, bool recursive = true)
        {
            try
            {
                IObservable<Item> observable = MinClient.ListObjectsAsync(bucketName, prefix, recursive);
                var result = new List<FileViewModel>();
                
                IDisposable subscription = observable.Subscribe(
                    item => result.Add(new FileViewModel
                        {
                         Name = item.Key,
                         Size = item.Size,
                         BucketName = bucketName
                        }),
                    ex => Console.WriteLine("OnError: {0}", ex),
                    () => Console.WriteLine("Listed all objects in bucket " + bucketName + "\n"));

                observable.Wait();
                subscription.Dispose();

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("[Bucket]  Exception: {0}", e);
                return new List<FileViewModel>();
            }
        }
        
        
        
        public void Download(string bucketName = "my-bucket-name", string objectName = "my-object-name", string fileName = "my-file-name")
        {
            try
            {
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("[Bucket]  Exception: {0}", e);
            }
        } 
    }
}