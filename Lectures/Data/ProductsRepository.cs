using System;
using System.Collections.Generic;
using System.Linq;
using Lectures.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Lectures.Data
{
    public class ProductsRepository
    {
        private readonly MongoClient _client;

        public ProductsRepository()
        {
            string username = "admin";
            string password = "root";
            string mongoDbAuthMechanism = "SCRAM-SHA-1";
            MongoInternalIdentity internalIdentity =
                new MongoInternalIdentity("admin", username);
            PasswordEvidence passwordEvidence = new PasswordEvidence(password);
            MongoCredential mongoCredential =
                new MongoCredential(mongoDbAuthMechanism,
                    internalIdentity, passwordEvidence);
            List<MongoCredential> credentials =
                new List<MongoCredential>() {mongoCredential};

            MongoClientSettings settings = new MongoClientSettings();
            // comment this line below if your mongo doesn't run on secured mode
            settings.Credentials = credentials;
            String mongoHost = "127.0.0.1";
            MongoServerAddress address = new MongoServerAddress(mongoHost);
            settings.Server = address;

            this._client = new MongoClient(settings);
        }

        public List<ProductViewModel> GetProducts()
        {
            var collection = this._client.GetDatabase("test").GetCollection<Product>("products");
            var products = collection.Find(new BsonDocument()).ToList();

            return products.Select(x => this.BsonToViewModel(x)).ToList();
        }

        public ProductViewModel GetProduct(string id)
        {
            var collection = this._client.GetDatabase("test").GetCollection<Product>("products");
            var document = new Product
            {
                Id = id
            };
            var filter = Builders<Product>.Filter.Eq("_id", new BsonObjectId(id));
            var productDocument = collection.Find(filter).FirstOrDefault();
            return this.BsonToViewModel(productDocument);
        }

        public void RemoveProduct(string id)
        {
            var collection = this._client.GetDatabase("test").GetCollection<Product>("products");
            var filter = Builders<Product>.Filter.Eq("_id", new BsonObjectId(id));
            collection.DeleteOne(filter);
        }

        public void CreateProduct(ProductViewModel product)
        {
            var productDocument = this.ViewModelToBson(product);
            var collection = this._client.GetDatabase("test").GetCollection<Product>("products");
            collection.InsertOne(productDocument);
        }

        public void UpdateProduct(ProductViewModel product)
        {
            var productDocument = this.ViewModelToBson(product);
            var collection = this._client.GetDatabase("test").GetCollection<Product>("products");
            var filter = Builders<Product>.Filter.Eq("_id", new BsonObjectId(productDocument.Id));
            var update = Builders<Product>.Update
                .Set("Name", product.Name)
                .Set("Description", product.Description)
                .Set("Price", product.Price);
            collection.UpdateOne(filter, update);
        }

        public Product ViewModelToBson(ProductViewModel product)
        {
            return new Product
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description
            };
        }

        public ProductViewModel BsonToViewModel(Product product)
        {
            return new ProductViewModel
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Description = product.Description
            };
        }
    }
}