﻿using Lectures.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lectures.Data
{
    public class UsersRepository
    {
        public static int _lastId = 8;

        public static ICollection<User> _users = new List<User>
        {
            new User {Id = 1, Name = "Ola", Login = "Olga1", CreatedAt = DateTime.Now.AddMonths(-13), CategoryID = 1},
            new User {Id = 2, Name = "Ala", Login = "Alga1", CreatedAt = DateTime.Now.AddMonths(-11), CategoryID = 2},
            new User {Id = 3, Name = "Bla", Login = "Blga1", CreatedAt = DateTime.Now.AddMonths(-10), CategoryID = 3},
            new User {Id = 4, Name = "Kla", Login = "Klga1", CreatedAt = DateTime.Now.AddMonths(-8), CategoryID = 3},
            new User {Id = 5, Name = "Sla", Login = "Slga1", CreatedAt = DateTime.Now.AddMonths(-4), CategoryID = 3},
            new User {Id = 6, Name = "Cla", Login = "Clga1", CreatedAt = DateTime.Now.AddMonths(-2), CategoryID = 4},
            new User {Id = 7, Name = "Xla", Login = "Xlga1", CreatedAt = DateTime.Now.AddMonths(-1), CategoryID = 4},
            new User {Id = 8, Name = "Zla", Login = "Zlga1", CreatedAt = DateTime.Now.AddMonths(-1), CategoryID = 4},
        };

        public ICollection<User> GetUsers()
        {
            return _users;
        }

        public User GetUser(int id)
        {
            return _users.Where(x => x.Id == id).First();
        }

        public void RemoveUser(int id)
        {
            var user = _users.Where(x => x.Id == id).FirstOrDefault();

            if (user == null)
                return;

            _users.Remove(user);
        }

        public void CreateUser(User user)
        {
            _lastId += 1;
            user.Id = _lastId;
            user.CreatedAt = DateTime.Now;
            _users.Add(user);
        }

        public void UpdateUser(EditUserViewModel user)
        {
            var existingUser = _users.Where(x => x.Id == user.Id).First();
            existingUser.Login = user.Login;
            existingUser.Name = user.Name;
            existingUser.CategoryID = user.CategoryId;
        }
    }
}
