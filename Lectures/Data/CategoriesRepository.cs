﻿using Lectures.Models;
using System.Collections.Generic;
using System.Linq;

namespace Lectures.Data
{
    public class CategoriesRepository
    {
        public static List<Category> _categories = new List<Category>
        {
            new Category {Id = 1, Name = "Administrator"},
            new Category {Id = 2, Name = "Editor"},
            new Category {Id = 3, Name = "Normal"}
        };

        public List<Category> GetCategories()
        {
            return _categories;
        }

        public Category GetCategory(int id)
        {
            return _categories.Where(x => x.Id == id).First();
        }

        public void RemoveCategory(int id)
        {
            var category = _categories.Where(x => x.Id == id).FirstOrDefault();

            if (category == null)
                return;

            _categories.Remove(category);
        }

        public void CreateCategory(Category category)
        {
            category.Id = _categories.Last().Id + 1;
            category.Name = category.Name;
        }

        public void UpdateCategory(Category category)
        {
            var existingCategory = _categories.Where(x => x.Id == category.Id).First();
            existingCategory.Name = category.Name;
        }
    }
}
