namespace Lectures.Models
{
    public class CatalogViewModel
    {
        public string Name { get; set; }
        public string CreatedAt { get; set; }
    }
}