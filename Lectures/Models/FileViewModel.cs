namespace Lectures.Models
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public string BucketName { get; set; }
        public ulong Size { get; set; }

        public string SizeMB => (Size / 1024).ToString() + " kB";
    }
}