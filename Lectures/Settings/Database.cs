namespace Lectures.Settings
{
    public class Database
    {
        public string ConnectionString { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}